# Introduction 
In our React apps we use react testing library for our unit/ function testing of the components. It creates a virtual DOM for testing and it also provides utilities for interacting with that DOM. This allows for components testing without a browser.

Cypress Components test runner enables you to run component tests in isolation, not just on an app or a page. It allows us to render components inside a browser, simulate user interactions and makes it easier to test/debug.  

Most tests will start with mount from @cypress/react. This is similar to render in Testing Library. With the components mounted in the browser, we can use already known Cypress assertions and Cypress API commands used in E2E tests, to test the components. 

Advantages:
1) Runs in a real browser and No jsdom.
2) We can see what is actually rendered in the browser.
3) Real user interactions in browser and close to actual user experience. 
4) In-browser debugging using devtools and DOM.
Cypress also has native support for retry-ability, 
5) Network-layer mocking with cy.intercept
6) One tool for component functional tests and E2E integration tests. 


The Cypress Component Testing library is still in Alpha. 

# Differences between E2E and Component Testing
Our Component Test Runner is separate from the Cypress End-to-End (E2E) Test Runner and they can be used either separately or together. They share the same driver, server, and commands.
Testing components requires less infrastructure than testing web applications; therefore, Cypress component tests are much, much faster than their E2E counterparts. Cypress component tests perform similarly to component tests executed within node-based runners like Jest and Mocha.

## Resources 
https://docs.cypress.io/guides/component-testing/introduction

Examples: https://github.com/cypress-io/cypress/tree/master/npm/react

## Pre-requisite 
A project with a package.json file at the root that runs on Webpack.

## Set up 
This project has integrated the Cypress compoenent runner with a REACT project. It also  supports other libraries like VUE, Webpack, Rollup*, and Vite*.

This project has used create-app-react project which is integrated with Webpack. 

  `npx create-react-app cypress-component-tests` 

Above command will create a new react project 

## Install dependencies

  `npm install --save-dev cypress @cypress/react @cypress/webpack-dev-server eslint-plugin-cypress`

You need eslint-plugin-cypress to `cy` global variable. 

Open the Cypress runner using below command to set up Cypress in the project.

`./node_modules/.bin/cypress open`

## Configuration 
Configure where all the compontents and tests are located in cypress.json 

```
    {
    "component": {
      "componentFolder": "src",
      "testFiles": "**/*.spec.ct.{js,jsx,ts,tsx}"
    }

```

Configure component testing plugin in the cypress/plugin/index.js - in our case react-script
This will configure the Cypress Webpack Dev Server to use the same Webpack configuration as Create React App uses. 

```
    module.exports = (on, config) => {
      if (config.testingType === 'component') {
           require('@cypress/react/plugins/react-scripts')(on, config)
      }
     return config
    }
```



## Write tests 
Create a folder for components inside /src folder and add a button component button.jsx

```
    import React from 'react'
    
    export default function Button({ action }) {
      return <button onClick={() => action()}>Call</button>
    }

```

Create Cypress component test for the above button component in the same folder.

```
    import { mount } from '@cypress/react'
    import Button from './button'

    describe('Invoke callback', () => {

        it('callback is called on button click', () => {
         const callback = cy.stub()
         mount(<Button action={callback} />)

         cy.contains('button', /call/i)
          .click()
          .then(() => {
             expect(callback).to.have.been.calledOnce
             expect(callback).to.have.been.calledWithExactly()
        })
      })
    })
    
```

### Run tests

Open the cypress components test-runner with 

` yarn cypress open-ct `

You can use `yarn cypress run-ct ` to run all the specs to use it 

## Components & test folders

This is a single page application where there are multiple components. 
All these components are inside the components folder and the tests are co-located in the same folder.

1) Greetings for the user - /src/components/userGreetings
  - to test context provider for isolated components 
2) Single course - /src/components/singleCourse
  - To test dynamic state(checked or not) 
  - Change of state (violet or green)
3) Single course component rendered multiple times based on the courses available - /src/components/multipleCourses
  - Component props
4) Accept Terms and condition and Enroll courses - /src/components/confirmCourses
  - mock functions
5) A text field to enter other courses, not specified above - /src/components/otherCourse 
6) Full app - /src/App

There is also a test at the page level in App.spec.ct.jsx.  

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.