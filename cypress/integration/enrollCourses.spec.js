/* eslint-disable jest/valid-expect */
describe("Tests for initial page - Enroll course", () => { 
it("Welcome Greeting with the username", () => { 

    cy.visit('http://localhost:3000')
    cy.get('#user-name')
    .should('contain', 'Sasikala Jayavel')
  
});

it('Other Courses', () => {
 
    cy.visit('http://localhost:3000')
    cy.contains('label', 'Other courses').click()
     
});

it("button colour should change when clicked on", () => { 
     
    cy.visit('http://localhost:3000')

    cy.get('#course-button')
     .should('be.enabled')
     .should('have.css', 'background-color')
     .and('eq', 'rgb(148, 0, 211)')

    cy.get('#course-button').click()
     .should('have.css', 'background-color')
     .and('eq', 'rgb(128, 0, 128)');    

});

it("select course should change the course button colour", () => { 
     
    cy.visit('http://localhost:3000')

    cy.get('#course-select-checkbox')
    .should('not.be.checked')
    .click()
    .should('be.checked')
    .should('be.enabled')

    cy.get('#course-button')
    .should('be.disabled')
    .should('have.css', 'background-color')
    .and('eq', 'rgb(0, 128, 0)');    

});

it('select multiple courses', () => { 
    const courseName = ['Cypress component testing', 'React testing Library', 'Unit testing using Jest']
    
    cy.visit('http://localhost:3000')
    cy.get('button#course-button').each(($li, index, list) => {
        expect(list).to.have.length(3);
        expect(list[index]).to.have.text(courseName[index])
      })
})

it('clicking on label gives focus to name input', () => {
    cy.visit('http://localhost:3000')

    cy.contains('label', 'Other courses').click()
    cy.get('input#name').should('have.focus')
  })

it('Click of confirm courses should call submitCourses', () => {
 
    cy.visit('http://localhost:3000')
    cy.get('#terms-and-conditions').click();
    cy.get('#confirm-courses').click()
    
    cy.get('@callBack')
      .should('have.been.calledOnce')
      .and('have.been.calledWithExactly', 'completed')

});
})