import { createContext, useContext } from "react"; 
const UserDetails = createContext();

// create custom hook to check whether we're inside a provider
 export function useUserDetails() {
  const context = useContext(UserDetails);

  if (!context) {
    throw new Error(
      "useUserDetails must be used within an UserDetailsProvider"
    );
  }
  return context;
}

export function UserDetailsProvider(props) { 
  const value = 'Sasikala Jayavel';
  return <UserDetails.Provider value={value} {...props} />;
} 

// eslint-disable-next-line import/no-anonymous-default-export
export default {UserDetailsProvider, useUserDetails}