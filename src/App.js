import './App.css';
import { useState }  from  'react'
import { UserDetailsProvider } from './context/UserContext';
import EnrollCourses from './components/enrollCourses'
import Container from 'react-bootstrap/Container'
import CoursesEnrolled from './components/coursesEnrolled/coursesEnrolled';
 
export default function App() {
  const [submitStatus, submitCourses] = useState('inProgress');

  let Component = EnrollCourses; // default to order page
  switch (submitStatus) {
    case 'inProgress':
      Component = EnrollCourses;
      break;
    case 'completed':
      Component = CoursesEnrolled;
      break;
    default:
  }

  return (
    <UserDetailsProvider>
      <Container>{<Component submitCourses={submitCourses} />}</Container>
    </UserDetailsProvider>
  );
}