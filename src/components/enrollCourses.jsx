import SelectCourseList from "./multipleCourses/courseList"
import OtherCourse from './otherCourse/otherCourse'; 
import UserFullName from './userGreetings/userFullName';
import ConfirmCourses from './confirmCourses/confirmCourses'

export default function EnrollCourses(submitCourses) {
    const courseName = ['Cypress component testing', 'React testing Library', 'Unit testing using Jest']
    return(
         
          <div className="app" >
            <div className="app-center">
                <UserFullName />
                <SelectCourseList courseNames={courseName} /> 
                <OtherCourse />
                <ConfirmCourses submitCourses={submitCourses}/>
            </div>
          </div>
         
      )
    }