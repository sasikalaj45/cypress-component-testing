import { useState } from 'react';

export default function SingleCourse({name}) {
  const [ buttonColor, setButtonColor ] = useState('darkViolet ');
  const [ disabled, setDisabled ] = useState(false);
  
  const newButtonColor = buttonColor === 'darkViolet ' ? 'purple' : 'darkViolet ';
  
  return (
    <div>
      <br></br>
      <input
        type="checkbox"
        id="course-select-checkbox"
        defaultChecked={disabled}
        aria-checked={disabled}
        onChange={(e) => setDisabled(e.target.checked)} 
        style={{ 'paddingRight': '20px', 'marginRight': 20 }}
      />
      <label htmlFor="disable-button-checkbox"></label>
      <button
         style={{backgroundColor: disabled ? 'green' : buttonColor, color: 'white', fontWeight: '600' }}
         onClick={() => setButtonColor(newButtonColor)}
         disabled={disabled}
         id='course-button'
       >{name}</button>
      <br></br>
    </div>  
  );
}