import { mount } from "@cypress/react"
import SingleCourse from "./singleCourse"
describe('Single Course loads properly', () => {

    it("button colour should change when clicked on", () => { 
        // to test dynamic state(checked or not) 
        // and change of state (violet or green)
    
        mount(<SingleCourse name="Cypress component testing"/>); 

        cy.get('#course-button')
         .should('be.enabled')
         .should('have.css', 'background-color')
         .and('eq', 'rgb(148, 0, 211)')
    
        cy.get('#course-button').click()
         .should('have.css', 'background-color')
         .and('eq', 'rgb(128, 0, 128)');    

    });

    it("select course should change the course button colour", () => { 
        // to test dynamic state( button checked or not) 
        // and change of state (violet or green)
        // selected options
        
        mount(<SingleCourse name="Contract testing using pact"/>); 
               
        cy.get('#course-select-checkbox')
        .should('not.be.checked')
        .click()
        .should('be.checked')
        .should('be.enabled')
    
        cy.get('#course-button')
        .should('be.disabled')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(0, 128, 0)');    
    
    });

})