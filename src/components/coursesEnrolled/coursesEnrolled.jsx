import Button from 'react-bootstrap/Button';
export default function CoursesEnrolled(submitCourses) { 
    function handleClick() {
        // send back to order page
        submitCourses('inProgress');
    }
    return (
    <div>
        <div>All your courses enrolled</div>
        <Button onClick={handleClick}>Return to Order Page</Button>
    </div>
    );  
}