/* eslint-disable jest/valid-expect */
import SelectCourseList from "./courseList";
import {mount} from "@cypress/react"

describe("Course List", () => { 
    //Component props
    const courseName = ['Cypress component testing', 'React testing Library', 'Unit testing using Jest']
    
    it('select multiple courses', () => { 
        mount(<SelectCourseList courseNames={courseName}/>)
        cy.get('button#course-button').each(($li, index, list) => {
            expect(list).to.have.length(3);
            expect(list[index]).to.have.text(courseName[index])
          })
    })
})