import React from 'react'; 
import SingleCourse from '../singleCourse/singleCourse';
import Row from "react-bootstrap/Row";

function SelectCourseList({ courseNames }) {
return ( 
    courseNames.map((courseName) => (    
     <div key={courseName}>
      <Row> 
        <SingleCourse  name={courseName}/>
        <br></br>
      </Row> 
     </div>      
    )
  ));
}
export default SelectCourseList; 