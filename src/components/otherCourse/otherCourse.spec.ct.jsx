import { mount } from '@cypress/react'
import OtherCourse from "../otherCourse/otherCourse"

describe('Focus is on correct element', () => {
  it('clicking on label gives focus to name input', () => {
    mount(<OtherCourse />)

    cy.contains('label', 'Other courses').click()
    cy.get('input#name').should('have.focus')
  })
})