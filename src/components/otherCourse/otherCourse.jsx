import React from 'react'

export default function OtherCourse() {
  return (
    <form>
      <label htmlFor="name" style={{'marginRight': 20, 'marginTop': '20px'}}>Other courses: </label>
      <input id="name" type="text" />
      
      <br></br>
    </form>
  )
}
