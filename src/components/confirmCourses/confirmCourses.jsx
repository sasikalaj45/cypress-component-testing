import React from 'react'; 
import  { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';

export default function ConfirmCourses({ submitCourses }) {
  const [tcChecked, setTcChecked] = useState(false);

  function handleClick() {
    submitCourses('completed');
  }

  const popover = (
    <Popover id="termsandconditions-popover">
      <Popover.Content>Sample Apps. No action will be taken</Popover.Content>
    </Popover>
  );
  
  const checkboxLabel = (
    <span>
         I agree to
        <OverlayTrigger placement="right" overlay={popover}>
            <span style={{ color: 'blue' }}> Terms and Conditions</span>
        </OverlayTrigger>
    </span>
  );

  return (
    <div>
        <Form>
            <Form.Group controlId="terms-and-conditions">
                <Form.Check
                    type="checkbox"
                    checked={tcChecked}
                    onChange={(e) => setTcChecked(e.target.checked)}
                    label={checkboxLabel}
                    style={{'marginRight': 20, 'marginTop': '40px' }}
                />
            </Form.Group>
        </Form>  
        <br></br>
        <Button  
             onClick={handleClick} 
             id='confirm-courses' variant="primary" type="submit" 
             style={{ backgroundColor: tcChecked? 'darkViolet': 'mediumPurple', fontColor: 'white', fontWeight: '600'}}
             disabled={!tcChecked} >
             Confirm selected courses
        </Button>
    </div>
     
  );
}
