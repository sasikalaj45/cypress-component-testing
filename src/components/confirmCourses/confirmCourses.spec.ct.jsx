import * as React from 'react'
import { mount } from '@cypress/react'
import ConfirmCourses from './confirmCourses'


it('Click of confirm courses should call submitCourses', () => {
 
    mount(<ConfirmCourses submitCourses={cy.stub().as('callBack')} />)
    cy.get('#terms-and-conditions').click();
    cy.get('#confirm-courses').click()
    
    
    cy.get('@callBack')
      .should('have.been.calledOnce')
      .and('have.been.calledWithExactly', 'completed')

});

