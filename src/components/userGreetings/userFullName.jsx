import { useUserDetails } from '../../context/UserContext'

export default function UserFullName() {
    const  user = useUserDetails(); 
    return (
    <div id='user-name' style={{'marginRight': 50, 'marginTop': '75px'}}>
      <p>Welcome {user}</p>
      <p>Please select the course you want to enroll</p>
    </div>
    )
}