import { mount } from "@cypress/react"
import { UserDetailsProvider } from "../../context/UserContext";
import UserFullName from "./userFullName"

it("Welcome Greeting with the username", () => { 
    // to test context provider for single components

    mount(
        <UserDetailsProvider value='Jayavel Sasikala'>
            <UserFullName />
        </UserDetailsProvider>
    ); 

    cy.get('#user-name')
    .should('contain', 'Jayavel Sasikala')
  
});