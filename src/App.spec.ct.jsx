import { mount } from '@cypress/react'
import App from "./App"

describe('All courses and other course load with theme', () => {
  it('clicking on label gives focus to name input', () => {

    mount(<App />)

    cy.contains('label', 'Other courses').click()
    cy.get('input#name').should('have.focus')
  })
})